public class Nodo {

	public String dato;
	public Nodo next;

	public Nodo(String valor) {
		dato = valor;
		next = null;
	}

	public Nodo(String valor, Nodo n) {
		dato = valor;
		next = n;
	}
 
}