public class Lista {

	public static Nodo primero;
	public static Nodo ultimo;

	public Lista() { 
		primero = null;
		ultimo = null;
	}
	public void insertarFinal(String elemento) {
		if(primero!= null){
			ultimo.next = new Nodo(elemento);
			ultimo = ultimo.next;
		}
		else
			ultimo = primero = new Nodo(elemento);
	}

	public void eliminarFinal() {
		Nodo tempo = primero;
		if(tempo != null){
			while(tempo.next != ultimo)
				tempo = tempo.next;

			ultimo = tempo;
			ultimo.next = null;
		}
	}

	public int tamanio() {
		int tama = 0;
		Nodo tempo = primero;
		while(tempo != null){	
			tama = tama + 1;
			tempo = tempo.next;
		}
		return tama;
	}

	public static Nodo getNodo() {
		return primero;
	}

}