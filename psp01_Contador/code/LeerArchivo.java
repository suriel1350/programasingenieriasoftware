import java.io.*; 
public class LeerArchivo {

	public static Lista leerFile() {
		// TODO - implement LeerArchivo.leerFile
		Lista myObj = new Lista();
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		
		try{
			archivo = new File("/home/suriel/Documentos/IngenieriaSoftware/psp_projectsIngSoft/psp01_D2/design/prueba2.java");
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			String linea;
			while(((linea=br.readLine())!=null)){
				myObj.insertarFinal(linea);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if(null != fr)
					fr.close();
			}catch (Exception e2){
				e2.printStackTrace();
			}
		}
		return myObj;
	}

}