public class Simpson 
{
    private double num_seg;
    private int tam;
    private double w;
    private double error;
    private double dof;
    private double x;
    private double[] xI;
    private double[] parteF1;
    private double[] parteF2;
    private double[] gammas;
    private double[] fX;
    private double[] multiplier;
    private double[] ops;

    public void set_num_seg(double valor)
    {
        num_seg = valor;
        tam = (int)num_seg + 1;
    }   

    public void set_w(double valor)
    {
        w = valor/num_seg;
        x = valor;
    }

    public void set_error(double valor)
    {
        error = valor;
    }

    public void set_dof(double valor)
    {
        dof = valor;
    }

    public void setXi()
    {
        xI = new double[tam];
        double suma  = 0;
        for(int i = 0; i < tam; i++)
        {
            xI[i] = suma;
            suma = suma + w; 
        }
    }

    public void setParte1F()
    {
        parteF1 = new double[tam];
        for(int i = 0; i < tam; i++)        
            parteF1[i] = 1 + ( (xI[i]*xI[i]) / dof );
        
    }

    public void setParte2F()
    {
        parteF2 = new double[tam];
        double potencia = (dof + 1) / 2;
        
        for(int i = 0; i < tam; i++)
            parteF2[i] = 1 / Math.pow(parteF1[i], potencia);
        
    }

    public void set_gamma()
    {
        gammas = new double[tam];
        double p1 = (dof + 1) / 2;
        double p2 = dof / 2;
        double gama1 = gamma(p1);
        double gama2 = gamma(p2);

        for(int i = 0; i < tam; i++)
            gammas[i] = gama1 / ( Math.sqrt(dof * Math.PI) * gama2 );
        
    }

    public void setFx()
    {
        fX = new double[tam];
        for(int i = 0; i < tam; i++)
            fX[i] = gammas[i] *parteF2[i];
        
    }

    public void setMultiplier()
    {
        multiplier = new double[tam];
        for(int i = 0; i < tam; i++)
        {
            if(i == 0 || i == tam - 1)
                multiplier[i] = 1;
            else
                if(i % 2 == 0)
                    multiplier[i] = 2;
                else
                    multiplier[i] = 4;
        }
    }

    public void operaciones()
    {
        ops = new double[tam];
        for(int i = 0; i < tam; i++)
            ops[i] = (w/3) * multiplier[i] * fX[i];
        
    }

    public double sumatoria()
    {
        double suma = 0;
        for(int i = 0; i < tam; i++)
            suma = suma + ops[i];
        
        return suma;
    }

    public double logGamma(double x) 
    {
        double tmp = (x - 0.5) * Math.log(x + 4.5) - (x + 4.5);
        double ser = 1.0 + 76.18009173    / (x + 0)   - 86.50532033    / (x + 1)
                       + 24.01409822    / (x + 2)   -  1.231739516   / (x + 3)
                       +  0.00120858003 / (x + 4)   -  0.00000536382 / (x + 5);
        return tmp + Math.log(ser * Math.sqrt(2 * Math.PI));
    }

    public double gamma(double x) 
    { 
        return Math.exp(logGamma(x)); 
    }
}