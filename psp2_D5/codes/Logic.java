public class Logic {

	private int n = 0;
	private double[] datosX;
	private double[] datosY;
	private double[] resultados;
	private double[][] data;
	private String[] arrData;
	private double media;
	private double desv;

	public void logicla() {
		// TODO - implement Logic.logicla
		Input myInput = new Input();
		Output myOut = new Output();
		Media myMedia = new Media();
		Simpson mySimpson = new Simpson();
		
		resultados = new double[3];
		data = myInput.readData("prueba1.txt");
		datosX = new double[data.length]; 
		datosY = new double[data.length];

		for(int i = 0; i < data.length; i++)
		{
			datosX[i] = data[i][0];
			datosY[i] = data[i][1];
		}

		for(int i = 0; i < data.length; i++)
		{
			mySimpson.set_num_seg(10);
			mySimpson.set_w(datosX[i]);
			mySimpson.set_error(0.00001);
			mySimpson.set_dof(datosY[i]);

			mySimpson.setXi();
			mySimpson.setParte1F();
			mySimpson.setParte2F();
			mySimpson.set_gamma();
			mySimpson.setFx();
			mySimpson.setMultiplier();
			mySimpson.operaciones();

			resultados[i] = mySimpson.sumatoria();
		}

		myOut.writeData("salida.txt","From 0 to x = 1.1 and dof = 9    p = " + resultados[0] +
									 "\nFrom 0 to x = 1.1812 and dof = 10    p = " + resultados[1] +
									 "\nFrom 0 to x = 2.750 and dof = 30    p = " + resultados[2]); 
	}

}
