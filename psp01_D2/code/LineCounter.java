import java.util.StringTokenizer;
public class LineCounter {
  
	public static int cantidadLineas;
	public static int cantidadClases;
	public static int cantidadFunciones;
	public static int cantidadComentarios;

	public static void analizarArchivo(Lista list, Nodo prim) {
		
		Nodo tempo = prim;
		int tamn;
		int comentarioAbre = 0;
	    int espacios = 0;
	    int esFuncion = 0;
	    int funcion = 0;
	    int comentarioCierra = 0;
		String linea;
		StringTokenizer tokens;
		String palabra;
		cantidadLineas = 0;
	    cantidadClases = 0;
	    cantidadFunciones = 0;
	    cantidadComentarios = 0;

		while(tempo != null){	        
	        linea = tempo.dato;
	        tokens = new StringTokenizer(linea);	                
	        tamn = tokens.countTokens();
	                
	                for(int contador = 0; contador < tamn; contador++){
	                    palabra = tokens.nextToken();

	                    if(palabra.equals("class")){
	                    	cantidadClases++;
	                    	cantidadLineas++;
	                    	break;
	                    }
	                    else{
	                    	if(palabra.equals("/*"))
	                    	{
	                    		cantidadComentarios++;
	                    		
	                    	}
	                    	
	                    	if(palabra.equals("*"))
	                    	{
	                    		cantidadComentarios++;
	                   		
	                    	}       
            	
	                    	
	                    	if(palabra.equals(" "))
	                    		espacios++;

	                    	if(palabra.equals("{"))
	                    		esFuncion = 1;

	                    	if(palabra.equals("public"))
	                    		funcion = 1;
	                    }
	                }

	     	if(esFuncion == 1 && funcion == 1)
	     	{
	     		cantidadFunciones++;
	     		esFuncion = 0;
	     		funcion = 0;
	     	}

	     	cantidadLineas++;
			tempo = tempo.next;
		}	
		System.out.println(espacios);
	}

	public static int getLineas() {
		return (cantidadLineas - cantidadComentarios) - 3;
	}

	public static int getClases() {
		return cantidadClases;
	}

	public static int getFunciones(){
		return cantidadFunciones;
	}

}