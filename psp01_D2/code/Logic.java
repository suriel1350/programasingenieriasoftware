public class Logic {

	public static void programa() { 
		Lista myList = LeerArchivo.leerFile();

		LineCounter.analizarArchivo(myList, myList.getNodo()); 
		int totalLineas = LineCounter.getLineas();
		int totalClases = LineCounter.getClases();
		int totalFunciones =  LineCounter.getFunciones();
		SalidaArchivo.writeData("resultados.txt", "Lineas = " + totalLineas + " Clases = " + totalClases
								+ " Funciones =  " + totalFunciones);
	}

} 