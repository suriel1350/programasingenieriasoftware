public class Logic {

	private int n = 0;
	private double[] datosX;
	private double[] datosY;
	private double[][] data;
	private String[] arrData;
	private double media;
	private double desv;

	public void logicla() {
		// TODO - implement Logic.logicla
		Input myInput = new Input();
		Output myOut = new Output();
		Data myData = new Data();
		Media myMedia = new Media();
		EstimacionCorLineal myProgress = new EstimacionCorLineal();
		
		data = myInput.readData("prueba4.txt");
		datosX = new double[data.length]; 
		datosY = new double[data.length];

		for(int i = 0; i < data.length; i++)
		{
			datosX[i] = data[i][0];
			datosY[i] = data[i][1];
		}

		myProgress.avgX(datosX);
		myProgress.avgY(datosY);

		myProgress.sumXX(datosX);
		myProgress.sumXY(datosX, datosY);
		myProgress.sumYY(datosY);
		
		/*arrData = myData.saveData(data);
		n = arrData.length;
		media = myMedia.getMedia(arrData,n);
		desv = myDesv.getDesvStd(arrData,media,n); */
		
		myOut.writeData("salida4.txt","B1 = " + myProgress.getB1() + "\nR^2 = " + myProgress.getR1() 
								+ "\nB0 = " + myProgress.getB0() + "\nRxy = " + myProgress.getR0() +
								"\n\nYk = " + myProgress.getYk()); 
	}

}
