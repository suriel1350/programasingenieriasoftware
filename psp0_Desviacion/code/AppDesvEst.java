public class AppDesvEst {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Lista myList = LeerArchivo.leerFile();
		double desviacionEstandar = DesviacionEstandar.getDesvEst(myList, Lista.getNodo());
		System.out.println("La desviacion estandar es: " + desviacionEstandar);
		double promedio = Promedio.getPromedio(myList, Lista.getNodo());
		System.out.println("El promedio es: " + promedio);
	}
}
