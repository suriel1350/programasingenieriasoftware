import java.io.*;
public class LeerArchivo 
{
	public static Lista leerFile() 
	{
		Lista myObj = new Lista();
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		
		try
		{
			archivo = new File("/home/suriel/Documentos/IngenieriaSoftware/psp_projects/psp0_Day1/design/PruebaDatos2.txt");
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			String numero;
			while(((numero=br.readLine())!=null))
			{
				double num = Double.parseDouble(numero);
				myObj.insertarFinal(num);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(null != fr)
					fr.close();
			}
			catch (Exception e2)
			{
				e2.printStackTrace();
			}
		}
		return myObj;
	}
}