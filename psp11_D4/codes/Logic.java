public class Logic {

	private int n = 0;
	private double[] data;
	private String[] arrData;
	private double media;
	private double desv;

	public void logicla() {
		// TODO - implement Logic.logicla
		Input myInput = new Input();
		Output myOut = new Output();
		RelativeSize mySizes = new RelativeSize();
		
		data = myInput.readData("prueba1.txt");

		mySizes.lnX(data);
		mySizes.avgLnXi();
		mySizes.getVar();
		mySizes.getDesvEst();
		mySizes.logs();
		mySizes.getVS();
		
		myOut.writeData("salida1.txt","VS = " + mySizes.getVS() + "\nS = " + mySizes.getS() 
								+ "\nM = " + mySizes.getM() + "\nL = " + mySizes.getL() +
								"\nVL = " + mySizes.getVL()); 
	}
}
