public class RelativeSize 
{
    
    private double xi = 0; // valor X del arreglo 
    private double ln = 0;
    private double[] lnXi; // variable para guardar el Ln de xi
    private double dblSumLnXi = 0; // suma Ln(xi)
    private double n;
    private double e = 2.7182;
    private double avg = 0; // Promedio n logarithmic values
    private double varianza = 0; // varianza de n datos
    private double desvEstandar = 0;
    private double lnVS, lnS, lnM, lnL, lnVL;

    public void lnX(double[] datalist) 
    {    
        lnXi = new double[datalist.length];
        n = (double)datalist.length;

        for(int i = 0; i < datalist.length; i++)
        {
            ln = Math.log(datalist[i]);
            dblSumLnXi = dblSumLnXi + ln;
            lnXi[i] = ln;
        }
    }

    public void avgLnXi()
    {
        avg = dblSumLnXi/n;
    }

    public void getVar()
    {
        DesviacionEstandar myVarianza = new DesviacionEstandar();
        varianza = myVarianza.getDesvEst(lnXi, avg);
    }

    public void getDesvEst()
    {
        desvEstandar = Math.sqrt(varianza);
    }

    public void logs()
    {
        lnVS = avg - (2*desvEstandar);
        lnS = avg - desvEstandar;
        lnM = avg;
        lnL = avg + desvEstandar;
        lnVL =  avg + (2*desvEstandar);
    }

    public double getVS()
    {
        double vS = Math.pow(e, lnVS);
        return vS;
    }
    
    public double getS()
    {
        double s = Math.pow(e, lnS);
        return s;        
    }

    public double getM()
    {
        double m = Math.pow(e, lnM);
        return m;        
    }

    public double getL()
    {
        double l = Math.pow(e, lnL);
        return l;        
    }

    public double getVL()
    {
        double vL = Math.pow(e, lnVL);
        return vL;        
    }
}