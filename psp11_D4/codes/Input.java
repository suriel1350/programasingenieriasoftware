import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Input {

	double[] numeros = new double[13];
	
	public double[] readData(String inFile) 
	{
        File f = new File(inFile);
        int i = 0, j = 0;
        double numero;
        StringTokenizer st;
        Scanner entrada = null;
        String cadena;
        try {
            entrada = new Scanner(f);
            while (entrada.hasNext()) 
            {
                cadena = entrada.nextLine();
                st = new StringTokenizer(cadena, " ");
                while (st.hasMoreTokens()) 
                {
                    numero = Double.parseDouble(st.nextToken());
                   	numeros[i] = numero;
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } finally {
            entrada.close();
        }

        return numeros;
    }
}
