public class DesviacionEstandar {

	public static double getDesvEst(double[] lnXI, double average) 
	{
		double n = (double)lnXI.length;
		double result;
		double cuadrado; 
		double suma = 0;
		double fin;

		for(int i = 0; i < lnXI.length; i++)
		{
			result = lnXI[i] - average;
			cuadrado = result*result;
			suma = suma + cuadrado;
		}

		fin = suma / (n - 1);
		return fin;
	}
}
