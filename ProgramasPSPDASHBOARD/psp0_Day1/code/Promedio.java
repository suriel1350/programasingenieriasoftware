public class Promedio {

	/**
	 * 
	 * @param list
	 * @param prim
	 */
	public static double getPromedio(Lista list, Nodo prim) 
	{
		Nodo tempo = prim;
		double suma = 0;
		while(tempo != null)
		{	
			suma = suma + tempo.dato;
			tempo = tempo.next;
		}		
		return (suma/list.tamanio());
	}
}
