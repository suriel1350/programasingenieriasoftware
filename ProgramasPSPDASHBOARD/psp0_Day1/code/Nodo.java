public class Nodo {

	public double dato;
	public Nodo next;

	/**
	 * 
	 * @param valor
	 */
	public Nodo(double valor) 
	{
		dato = valor;
		next = null;
	}

	/**
	 * 
	 * @param valor
	 * @param n
	 */
	public Nodo(double valor, Nodo n) 
	{
		dato = valor;
		next = n;	
	}

}