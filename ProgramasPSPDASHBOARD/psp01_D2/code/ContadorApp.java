/*
* %W% %E% Suriel Asael Rosas Mendez
*
* Copyright (c) 1993-1996 ITESM CCV, Inc. All Rights Reserved.
*
* This software is the confidential and proprietary information of Sun
* Microsystems, Inc. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Sun.
*
* SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
* THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
* ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
* DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/

/*
* Class description goes here.
*
* @version 1.10 19 Febrero 2016
* @author Suriel Asael Rosas Mendez
*/
public class ContadorApp {
 	/* Clase principal que llamara a la funcion programa de la clase Logic*/

	public static void main(String[] args) {	
		Logic.programa(); 
	}

}