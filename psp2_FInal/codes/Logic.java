public class Logic {

	private int n = 0;
	private double[] datosW;
	private double[] datosX;
	private double[] datosY;
	private double[] datosZ;
	private double[] finales;
	private double[][] data;
	private double media;
	private double desv;

	public void logicla() {
		// TODO - implement Logic.logicla
		Input myInput = new Input();
		Output myOut = new Output();
		Data myData = new Data();
		Media myMedia = new Media();
		Regresion myProgress = new Regresion();
		
		data = myInput.readData("prueba1.txt");
		datosW = new double[data.length];
		datosX = new double[data.length]; 
		datosY = new double[data.length];
		datosZ = new double[data.length];

		for(int i = 0; i < data.length; i++)
		{
			datosW[i] = data[i][0];
			datosX[i] = data[i][1];
			datosY[i] = data[i][2];
			datosZ[i] = data[i][3];
		}

		myProgress.sumas(datosW, datosX, datosY, datosZ);
		myProgress.gauss();
		myProgress.varianza(datosW, datosX, datosY, datosZ);
		myProgress.rango(datosW, datosX, datosY, datosZ);
		finales = myProgress.calculaZ();
		
		
		myOut.writeData("salida.txt","B0 = " + finales[0] + "\nB1 = " + finales[1] 
								+ "\nB2 = " + finales[2] + "\nB2 = " + finales[3] +
								"\nProjected Hours = " + finales[4] + "\nUPI = " + finales[5] + "\nLPI = " + finales[6]); 
	}

}
