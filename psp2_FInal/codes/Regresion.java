public class Regresion
{
    private int intN;
    public double dblSumW = 0;
    public double dblSumX = 0;
    public double dblSumY = 0;
    public double dblSumZ = 0;
    public double getAvgW = 0;
    public double getAvgX = 0;
    public double getAvgY = 0;
    public double getAvgZ = 0;
    public double dblSumWCuadrado = 0;
    public double dblSumXCuadrado = 0;
    public double dblSumYCuadrado = 0;
    public double wX = 0;
    public double wY = 0;
    public double wZ = 0;
    public double xY = 0;
    public double xZ = 0;
    public double yZ = 0;   
    public double b0;
    public double b1;     
    public double b2;     
    public double b3;  
    public double varianza = 0;  
    public double valorX = 0; 
    public double rang = 0;


    public void sumas(double[] datalistW, double[] datalistX, double[] datalistY, double[] datalistZ) 
    {
        intN = datalistW.length;
        for(int i = 0; i < intN; i++)
        {
            dblSumW = dblSumW + datalistW[i];
            dblSumX = dblSumX + datalistX[i];
            dblSumY = dblSumY + datalistY[i];
            dblSumZ = dblSumZ + datalistZ[i];

            dblSumWCuadrado = dblSumWCuadrado + ( datalistW[i] * datalistW[i]);
            dblSumXCuadrado = dblSumXCuadrado + ( datalistX[i] * datalistX[i]);
            dblSumYCuadrado = dblSumYCuadrado + ( datalistY[i] * datalistY[i]);

            wX = wX + (datalistW[i] * datalistX[i]);
            wY = wY + (datalistW[i] * datalistY[i]);
            wZ = wZ + (datalistW[i] * datalistZ[i]);
            xY = xY + (datalistX[i] * datalistY[i]);
            xZ = xZ + (datalistX[i] * datalistZ[i]);
            yZ = yZ + (datalistY[i] * datalistZ[i]);

        }

        getAvgW = dblSumW / intN;
        getAvgX = dblSumX / intN;
        getAvgY = dblSumY / intN;
        getAvgZ = dblSumZ / intN;
    }

    public void gauss()
    {
        GaussJordan myMatriz = new GaussJordan();
        
        double matriz[][] = new double[4][5];
        double resultados[][];
        matriz[0][0] = 6; matriz[0][1] = dblSumW; matriz[0][2] = dblSumX; matriz[0][3] = dblSumY; matriz[0][4] = dblSumZ; 
        matriz[1][0] = dblSumW; matriz[1][1] = dblSumWCuadrado; matriz[1][2] = wX; matriz[1][3] = wY; matriz[1][4] = wZ;
        matriz[2][0] = dblSumX; matriz[2][1] = wX; matriz[2][2] = dblSumXCuadrado; matriz[2][3] = xY; matriz[2][4] = xZ;
        matriz[3][0] = dblSumY; matriz[3][1] = wY; matriz[3][2] = xY; matriz[3][3] = dblSumYCuadrado; matriz[3][4] = yZ;
        
        resultados = myMatriz.resuelve(matriz);
        b0 = resultados[0][0];
        b1 = resultados[1][0];
        b2 = resultados[2][0];
        b3 = resultados[3][0];
    }

    public void varianza(double[] datalistW, double[] datalistX, double[] datalistY, double[] datalistZ)
    {
        double sum = 0;
        double calcula = 0;
        double cuadrado = 0;
        for(int i = 0; i < intN; i++)
        {   
            calcula = (datalistZ[i] - b0 - (b1*datalistW[i]) - (b2*datalistX[i]) - (b3*datalistY[i]) );
            cuadrado = calcula * calcula;
            sum = sum + cuadrado;
        }
        sum = sum / (intN - 4);
        varianza = Math.sqrt(sum);
    }

    public void rango(double[] datalistW, double[] datalistX, double[] datalistY, double[] datalistZ)
    {
        valorX = 1.386;
        double newW = 650 - getAvgW;
        newW = newW * newW;

        double newX = 3000 - getAvgX;
        newX = newX *  newX;

        double newY = 155 - getAvgY;
        newY = newY * newY;

        double sumatoriaW = 0, sumatoriaX = 0, sumatoriaY = 0, sumatoriaZ = 0;
        double temporal = 0;
        for(int i = 0; i < intN; i++)
        {
            temporal = (datalistW[i] - getAvgW);
            sumatoriaW = sumatoriaW + (temporal * temporal);

            temporal = (datalistX[i] - getAvgX);
            sumatoriaX = sumatoriaX + (temporal * temporal);

            temporal = (datalistY[i] - getAvgY);
            sumatoriaY = sumatoriaY + (temporal * temporal);
        }
        
        rang = valorX * varianza * Math.sqrt( 1 + (1/intN) +  (newW/sumatoriaW) + (newX/(int)sumatoriaX) + (newY/sumatoriaY));
        rang = rang + 2;
    }   

    public double[] calculaZ()
    {
        double valorZ =  (b0 + (650*b1) + (3000*b2) + (155*b3));
        double upi = valorZ + rang;
        double lpi = valorZ - rang;

        double resultados[] = new double[7];
        resultados[0] = b0;
        resultados[1] = b1;
        resultados[2] = b2;
        resultados[3] = b3;
        resultados[4] = valorZ;
        resultados[5] = upi;
        resultados[6] = lpi;

        return resultados;        
    }
}