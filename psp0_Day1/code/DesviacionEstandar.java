public class DesviacionEstandar {

	/**
	 * 
	 * @param list
	 * @param prim
	 */
	public static double getDesvEst(Lista list, Nodo prim) 
	{
		int n = list.tamanio();
		double result;
		double cuadrado;
		double suma = 0;
		double suma2 = 0;
		double fin;
		double prom;
		int cont = 0;

		Nodo tempo = prim;
		Nodo tempo2 = prim;
		while(tempo != null)
		{
			while(tempo2 != null)
			{
				suma = suma + tempo2.dato;
				tempo2 = tempo2.next;
			}
			tempo2 = prim;
			prom = suma/n;
			suma = 0;
			result = (prom - tempo.dato);
			cuadrado = result*result;
			suma2 = suma2 + cuadrado;
			tempo = tempo.next;
		}

		fin = Math.sqrt((suma2/(n-1)));
		return fin;
	}
}
