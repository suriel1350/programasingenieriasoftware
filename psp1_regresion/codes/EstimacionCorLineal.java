public class EstimacionCorLineal 
{
    
    private double dblSumX = 0; // suma X del arreglo 
    private double dblSumY = 0; // suma Y del arreglo
    private double dblSumXX = 0; // suma X^2 
    private double dblSumXY = 0; // suma X * Y
    private double dblSumYY = 0; // suma Y^2 
    private int intN;
    private double getAvgX = 0;
    private double getAvgY = 0;
    public int intLoc;
    public double dblB1 = 0;  // valor Formula B1
    public double dblR1 = 0;
    public double dblR0 = 0;
    public double dblB0 = 0;
    public double dblYk = 0;

    public void sumXX(double[] datalist) {
        for(int i = 0; i < datalist.length; i++)
            dblSumXX = dblSumXX + ( datalist[i] * datalist[i] );
    }

    public void sumXY(double[] datalistX, double[] datalistY) {
        for(int i = 0; i < datalistX.length; i++)
            dblSumXY = dblSumXY + ( datalistX[i] * datalistY[i] );
    }

    public void sumYY(double[] datalist) {
        for(int i = 0; i < datalist.length; i++)
            dblSumYY = dblSumYY + ( datalist[i] * datalist[i] ); 
    }

    public void avgX(double[] datalist) 
    {
        intN = datalist.length;
        for(int i = 0; i < datalist.length; i++)
            dblSumX = dblSumX + datalist[i];
    
        getAvgX = dblSumX / datalist.length;
    }

    public void avgY(double[] datalist) {
        for(int i = 0; i < datalist.length; i++)
            dblSumY = dblSumY + datalist[i];

        getAvgY = dblSumY / datalist.length;
    }

    public double getB1() {
        double v1 = ( (dblSumXY) - (intN * getAvgX * getAvgY) ) ;
        double v2 = ( (dblSumXX) - (intN * (getAvgX * getAvgX) ) );
        dblB1 = v1 / v2; 
        return dblB1;
    }

    public double getR1() {
        long v1 = ( intN * (int)dblSumXY )- ( (int)dblSumX * (int)dblSumY );
        long v2 = ( intN * (int)dblSumXX )- ( (int)dblSumX * (int)dblSumX );
        long v3 = ( intN * (int)dblSumYY )- ( (int)dblSumY * (int)dblSumY );
        long v4 = v2 * v3;
        int v5 = (int)Math.sqrt(v4);
        dblR1 =  (double)v1 / (double)v5;
        dblR0 = dblR1;
        dblR1 = dblR1 * dblR1; 
        return dblR1;
    }

    public double getR0(){
        return dblR0;
    }

    public double getB0() {
        dblB0 = getAvgY - (dblB1*getAvgX);
        return dblB0;
    }

    public double getYk() {
        dblYk = dblB0 + (dblB1*386);
        return dblYk;
    }
}